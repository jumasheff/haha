import React from "react";
import { connect } from "react-redux";
import Editor from './Editor';
import Console from './Console';
import WebTerminal from './WebTerminal';
import { write, error, flush, exit } from "../../actions/ideActions";


class IDE extends React.Component {
    
    render() {
        return [
            <div className="ide" key="ide">
                <Editor
                    write={this.props.write}
                    error={this.props.error}
                    flush={this.props.flush}
                    exit={this.props.exit}
                />
                <Console
                    lines={this.props.lines}
                    errorObj={this.props.errorObj}
                />
            </div>,
            <div className="todo" key="todo">
            <WebTerminal />
                TODO:
                <ul>
                    <li>Display exercises</li>
                    <li>Store progress of a registered user</li>
                    <li>Gamify</li>
                    <li>Enable smart console (xterm)</li>
                    <li>Inject asserts/code validation</li>
                    <li>i18n</li>
                    <li>Multi-file projects (display files tree)</li>
                    <li>Run code on the backend on a per-user-created-docker-container</li>
                    <li>Create a fully featured django course</li>
                    <li>Tutors/instructors marketplace</li>
                </ul>
            </div>
        ]
    }    
}

function mapStateToProps(state) {
    return {
        lines: state.ide.lines,
        errorObj: state.ide.error,
    }
}

export default connect(mapStateToProps, { write, error, flush, exit } )(IDE);
