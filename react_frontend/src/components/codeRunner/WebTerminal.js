import axios from 'axios'
import * as throttle from 'lodash.throttle'
import React from "react"
import { ResizableBox } from 'react-resizable'
import 'xterm/src/xterm.css'
import 'xterm/src/addons/fullscreen/fullscreen.css'
import 'xterm/dist/addons/terminado/terminado.js';
import 'xterm/dist/addons/fit/fit.js';
import { terminadoAttach } from './addons/terminado.js'
import XTerm from './XTerm.js'


class WebTerminal extends React.Component {
  
  componentDidMount() {
    runFakeTerminal(this.refs.xterm);
  }

  resize = () => {
    // TODO(murat): Use xterm.resize(cols, rows)
    this.refs.xterm && this.refs.xterm.fit()
  }

  throttleConsoleResize = (size)  => {
    throttle(resize, 50)
  }

  render() {
    // TODO(murat): Resizing doesn't work.
    return <div>
      <ResizableBox width={200} height={200} onResize={this.throttleConsoleResize} style={{
        overflow: 'hidden'
      }}>
				<XTerm ref='xterm' style={{
					addons:['terminado', 'fit', 'fullscreen', 'search'],
					overflow: 'hidden',
					position: 'relative',
					width: '100%',
					height: '100%'
				}} />
      </ResizableBox>
    </div>
  }
}

function runFakeTerminal(xterm) {
  const shellprompt = '$ '
  const prompt = () => {
    xterm.write('\r\n' + shellprompt)
  }
  const term = xterm.getTerminal()

  xterm.writeln('Welcome to oku.kg')
  xterm.writeln('')
  prompt()

  // TODO(murat): Get terminal name from backend
  const terminalName = Math.random().toString(36).substring(7)
  axios.get(`http://localhost:8700/new_json/${terminalName}`)
    .then(r => {
      const wsUrl = r.data.url
      const ws = new WebSocket(`ws://localhost:8700${wsUrl}`)
      ws.onopen = () => {
        terminadoAttach(term, ws, true)
        // TODO(murat): Send resize command
        term.on('paste', function (data, ev) {
          xterm.write(data);
        });
      }
    })
}

export default WebTerminal;
