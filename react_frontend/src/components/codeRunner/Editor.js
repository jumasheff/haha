import React from "react";
import AceEditor from "react-ace";
import brace from "brace";
import { debounce } from "lodash";

import "brace/mode/python";
import "brace/theme/terminal";

import Worker from '../../worker/skulpt.worker';


class Editor extends React.Component{
    constructor(props) {
        super(props);
        this.state = { buffer: 'print "Hello world"\n' }
    }

    componentDidMount() {
        this.lazyRun = debounce(this.runWorker, 300);
    }

    handleLoad(editor) {
        editor.focus();
    }

    runWorker() {
        const write = this.props.write;
        const error = this.props.error;
        const flush = this.props.flush;
        const exit = this.props.exit;
        const code = this.state.buffer;
        const worker = new Worker();

        worker.onmessage = function(event) {
            const result = JSON.parse(event.data);
            if(result.type === 'exit') exit(worker)
            if (result.type === 'stderr') error(result.data)
            if (result.data && result.type !== 'stderr') write(result.data)
        }
        flush();
        worker.postMessage(code)
    }

    handleChange(buffer) {
        this.setState({ buffer: buffer })
        // implement hot code running toggle
        // this.lazyRun();
    }

    render() {
        return (
            <div className="editor">
                <AceEditor
                    key="editor"
                    mode={"python"}
                    theme={"terminal"}
                    name="codeEditor"
                    onLoad={this.handleLoad.bind(this)}
                    onChange={this.handleChange.bind(this)}
                    value={this.state.buffer}
                    fontSize={16}
                    style={{height: '500px'}}
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={true}
                    setOptions={{
                        enableBasicAutocompletion: true,
                        enableLiveAutocompletion: true,
                        enableSnippets: true,
                        showLineNumbers: true,
                        tabSize: 4,
                }}/>
                <button className="submit" key="btn" onClick={this.runWorker.bind(this)}>Submit</button>
            </div>
        )
    }
}

export default Editor;
