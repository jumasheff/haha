# Excercises structure

## Tracks
Tracks examples:
 * Python
 * JavaScript
 * React
 * etc.

## Units
Units are the building blocks of Tracks. For example, Python Track can have several Units:
 * Beginners Guide
 * Loops
 * Functions
 * Object Oriented Programming
 * External Libraries
 * etc.

## Lessons
Lessons are the building blocks of Units. For example, Beginners Guide can consist of several Lessons:
 * Simple math exercises
 * Working with strings
 * Built-in data structures
 * etc.

### Exercises
Exercises are the materials users will see in their web-based consoles. Most of the exercises contain a challenge, for example "add a missing line of code", or "write a loop to print out all characters in a given string".

<img src="./courses_structure.png" alt="Courses structure" width="500">
