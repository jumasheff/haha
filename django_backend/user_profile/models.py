from django.db import models
from django.conf import settings
from authtools.models import AbstractEmailUser


class User(AbstractEmailUser):
    username = models.CharField(
        'Username', max_length=255, null=True, blank=True)

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __str__(self):
        return '<{}>'.format(self.email)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    # custom fields for user
    website = models.URLField(blank=True, null=True)
    about = models.CharField(max_length=255, blank=True, null=True)
