from django.contrib.sites.shortcuts import get_current_site
from allauth.account.adapter import DefaultAccountAdapter


class CustomAccountAdaper(DefaultAccountAdapter):
    def clean_username(self, username, shallow=False):
        # TODO(murat): change this if we need to validate usernames
        return username


class MyAccountAdapter(CustomAccountAdaper):

    def get_email_confirmation_url(self, request, emailconfirmation):
        current_site = get_current_site(request)
        return '{}/account/confirm-email/{}/'.format(current_site, emailconfirmation.key)
